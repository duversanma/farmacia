/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia;

//import com.sun.jdi.connect.spi.Connection;
//import java.beans.Statement;

import java.sql.*;
import java.util.logging.*;

/**
 *
 * @author Sanma
 */
public class ConexionBD {

    // Configuracion de la conexion a la base de datos
    // Atributos
    private String url = "";
    public Connection con = null;
    // Sirve para procesar una sentencia SQL QUERY/consulta
    private Statement stmt = null;
    // Es un objeto que proporciona varios métodos para obtener los datos de una columna correspondiente a una fila
    private ResultSet rs = null;

    // Cosntructor
    // throws: Sirve para pasar la excepción hacia arriba de la función.
    // El throws permite lanzar un método tengamos o no un error
    public ConexionBD() throws ClassNotFoundException{
        url = "C:/Users/Sanma/Documents/NetBeansProjects/SanmaReto052/SanmaReto052.db";
        try {// Realizamos la conexión a la BD
            // Registra el Driver de conexión para la base de datos
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:" + url);
            if (con != null) { // Valide si hay conexión
                DatabaseMetaData meta = con.getMetaData();
                System.out.println("Base de datos conectada :D " + meta.getDriverName());
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    } 
    //Retornar la conexión
    public Connection getConnection() {
        return con;
    }
    
    //Cerrar la conexión
    public void closeConnection(Connection con) {
        if (con != null) { // Si hay conexión a la BD
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    // Método que devuelve un ResultSet de una consulta (tratamiento de SELECT)
    public ResultSet consultarBD(String sentencia) {
        try {
            stmt = con.createStatement();// Procesa la consulta
            rs = stmt.executeQuery(sentencia); // Ejecuta la consulta
        } catch (SQLException sqlex) {
            System.out.println(sqlex.getMessage());
        } catch (RuntimeException rex) {
            System.out.println(rex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return rs;
    }
    
    // Metodo que realiza un INSERT y devuelve TRUE si la operacin fue existosa
    public boolean insertarBD(String sentencia) {
        try {
            stmt = con.createStatement(); //Procesa la consulta
            stmt.execute(sentencia); // Ejecuta la consulta
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR INSERCIÓN: " + sqlex);
            return false;
        }
        return true;
    }

    public boolean borrarBD(String sentencia) {
        try {
            stmt = con.createStatement();//Procesa la consulta
            stmt.execute(sentencia);// Ejecuta la consulta
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR AL ELIMINAR: " + sqlex);
            return false;
        }
        return true;
    }
// Método que realiza una operacin como UPDATE, DELETE, CREATE TABLE, entre otras
// y devuelve TRUE si la operacin fue existosa

    public boolean actualizarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }

    // Confirmar cambios a la BD
    public boolean setAutoCommitBD(boolean parametro) {
        try {
            con.setAutoCommit(parametro); // Confirma conectado a la BD si realizó un cambio o no
        } catch (SQLException sqlex) {
            System.out.println("Error al configurar el autoCommit " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public void cerrarConexion() { // Ejecuta el método closeConnection creado anteriormente
        closeConnection(con);
    }

    public boolean commitBD() {
        try {
            con.commit();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer commit " + sqlex.getMessage());
            return false;
        }
    }

    public boolean rollbackBD() { // Reversión de una operación en la BD de algún estado anterior
        // Su función principal es que se pueda restaurar la BD a una copia limpia incluso después
        // de realizar una operación erronea
        try {
            con.rollback();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer rollback " + sqlex.getMessage());
            return false;
        }
    }
    

}